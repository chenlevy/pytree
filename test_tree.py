#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
:author: Chen Levy <chen (at) pytree.org>
:license: MIT
:date: 2013-03-11
"""

import unittest
import StringIO
from tree import *

class Test_tree(unittest.TestCase):

    def setUp(self):
        self.out = StringIO.StringIO()

        self.tree_input = [
            "a",
            "b/ba",
            "b/bb",
            "c/ca/caa",
            "c/cb" ]

        self.tree_data = (
            '/', [                     # /
                ('a',[]),              # |-- a
                ('b',[                 # |-- b
                    ('ba',[]),         # |   |-- ba
                    ('bb',[]) ]),      # |   `-- bb
                ('c',[                 # `-- c
                    ('ca',[            #     |-- ca
                        ('caa',[]) ]), #     |   `-- caa
                    ('cb',[]) ]) ])    #     `-- cb

        self.tree_output = '\n'.join([
            "/",
            "|-- a",
            "|-- b",
            "|   |-- ba",
            "|   `-- bb",
            "`-- c",
            "    |-- ca",
            "    |   `-- caa",
            "    `-- cb",
            ""])

    def test_init(self):
        self.assertEqual( Tree().data, ('/',[]) )
        self.assertEqual( Tree(sep='@').data, ('@',[]) )

    def test_add_branch(self):
        tree = Tree()
        self.assertEqual( tree.data, ('/',[]))
        tree += 'a/b/c'
        self.assertEqual( tree.data, ('/',[('a',[('b',[('c',[])])])]))

    def test_add_branch_alt_sep(self):
        tree = Tree(sep='@') + 'a@b@c'
        self.assertEqual( tree.data, ('@',[('a',[('b',[('c',[])])])]))

    def test_add_tree(self):
        t1 = Tree('/') + 'a/b/c' + 'a/d/e'
        self.assertEqual( t1.data,
                          ('/',[
                              ('a',[
                                  ('b',[
                                      ('c',[])]),
                                  ('d',[
                                      ('e',[])])])]) )
        t2 = Tree('/') + 'a/b/c' + 'x/y/z'
        self.assertEqual( t2.data,
                          ('/',[
                              ('a',[
                                  ('b',[
                                      ('c',[])])]),
                              ('x',[
                                  ('y',[
                                      ('z',[])])])]) )
        self.assertEqual( (t1 + t2).data,
                          ('/',[
                              ('a',[
                                  ('b',[
                                      ('c',[])]),
                                  ('d',[
                                      ('e',[])])
                              ]),
                              ('x',[
                                  ('y',[
                                      ('z',[])])])]) 
                      )

    def test_str(self):
        tree = Tree()
        for i in self.tree_input:
            tree += i
        self.assertEqual( '%s' % tree, self.tree_output )

    def test_read(self):
        tree = Tree()
        tree.read(self.tree_input)
        self.assertEqual( tree.data, self.tree_data )

    def test_write(self):
        tree = Tree()
        tree.read(self.tree_input)
        out = StringIO.StringIO()
        tree.write(out)
        out.seek(0)
        self.assertEqual( out.read(), self.tree_output )
        
if __name__ == '__main__':
    unittest.main()
