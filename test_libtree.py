#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
:author: Chen Levy <chen (at) pytree.org>
:license: MIT
:date: 2013-03-11
"""

import unittest
import StringIO
from libtree import *

class Test_libtree(unittest.TestCase):

    TREE_INPUT = [
            "a",
            "b/ba",
            "b/bb",
            "c/ca/caa",
            "c/cb" ]

    TREE_DATA = (
            '/', [                     # /
                ('a',[]),              # |-- a
                ('b',[                 # |-- b
                    ('ba',[]),         # |   |-- ba
                    ('bb',[]) ]),      # |   `-- bb
                ('c',[                 # `-- c
                    ('ca',[            #     |-- ca
                        ('caa',[]) ]), #     |   `-- caa
                    ('cb',[]) ]) ])    #     `-- cb

    TREE_OUTPUT = '\n'.join([
            "/",
            "|-- a",
            "|-- b",
            "|   |-- ba",
            "|   `-- bb",
            "`-- c",
            "    |-- ca",
            "    |   `-- caa",
            "    `-- cb",
            ""])


    def setUp(self):
        self.out = StringIO.StringIO()

    def test_segment_to_branch(self):
        f = segments_to_branch
        self.assertEqual( f(['a']),     ('a',[]) )
        self.assertEqual( f(['a','b']), ('a',[('b',[])]) )

    def test_create_branch(self):
        f = create_branch
        self.assertEqual( f('/'),   ('/',[]) )
        self.assertEqual( f('@',sep='@'), ('@',[])  )
        self.assertEqual( f('/a'),  ('/',[('a',[])]) )
        self.assertEqual( f('a'),   ('/',[('a',[])]) )
        self.assertEqual( f('a/b'), ('/',[('a',[('b',[])])]) )

    def test_merge_branch(self):
        f = merge
        tree = create_branch('/')
        self.assertEqual(tree, ('/',[]))
        f(tree, create_branch('a'))
        self.assertEqual(tree, ('/',[('a',[])]))
        f(tree, create_branch('b'))
        self.assertEqual(tree, ('/',[('a',[]),('b',[])]))
        f(tree, create_branch('a/c'))
        self.assertEqual(tree, ('/',[('a',[('c',[])]),('b',[])]))

    def test_merge_tree(self):
        f = merge
        tree1 = create_branch('/')
        for s in [ 'a/c', 'b']:
            f(tree1, create_branch(s))
        self.assertEqual(tree1,
                         ('/',[
                             ('a',[
                                 ('c',[])]),
                             ('b',[])]))

        tree2 = create_branch('/')
        for s in [ 'a/d/e', 'a/d/f', 'g' ]:
            f(tree2, create_branch(s))
        self.assertEqual(tree2,
                         ('/',[
                             ('a',[
                                 ('d',[
                                     ('e',[]),
                                     ('f',[])])]),
                             ('g',[])]))

        f(tree1, tree2)
        self.assertEqual(tree1,
                         ('/',[
                             ('a',[
                                 ('c',[]),
                                 ('d',[
                                     ('e',[]),
                                     ('f',[])])]),
                             ('b',[]),
                             ('g',[])]))

    def test_more_items(self):
        g = more_items(['foo', 42, 3.14])
        self.assertEqual( g.next(), ('foo', True) )
        self.assertEqual( g.next(), (42, True) )
        self.assertEqual( g.next(), (3.14, False) )
        self.assertRaises( StopIteration, g.next)

    def test_segment_str(self):
        f = segment_str
        self.assertEqual( f(True),  '|   ')
        self.assertEqual( f(False), '    ')

    def test_stem_str(self):
        f = stem_str
        self.assertEqual( f(True),  '|-- ')
        self.assertEqual( f(False), '`-- ')

    def test_base_str(self):
        f = base_str
        self.assertEqual( f([True,True,True]),  '|   |   |-- ')
        self.assertEqual( f([False,False]),     '    `-- ')
        self.assertEqual( f([True,False,True]), '|       |-- ')

    def test_write_tree(self):
        write_tree( self.TREE_DATA, self.out)
        self.out.seek(0)
        self.assertEqual( self.out.read(), self.TREE_OUTPUT )

    def test_repr(self):
        pass

    def test_read_tree(self):
        self.assertEqual( read_tree(self.TREE_INPUT),
                          self.TREE_DATA)

    def test_text_to_tree(self):
        text_to_tree(input=self.TREE_INPUT,
                     output=self.out,
                     sep='/')
        self.out.seek(0)
        self.assertEqual(self.out.read(), self.TREE_OUTPUT)

if __name__ == '__main__':
    unittest.main()
