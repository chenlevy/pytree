.. -*- mode: rst -*-
:title: Python tree utils
:author: Chen Levy <chen (at) pytree.org>
:license: MIT
:abstract: The output of tree(1) is useful, but its' input is limited
           to a structure of an accessible file-system. This project
           allow to generate such an output a list of delimited strings.

============================
 pytree - Python Tree Utils
============================

The problem is described in this question_ on stackoverflow.com. The
functionality is best demonstrated with an example:

Example
=======

Input::

  fruit%apple%green
  fruit%apple%red
  fruit%apple%yellow
  fruit%banana%green
  fruit%banana%yellow
  fruit%orange%green
  fruit%orange%orange
  i_want_my_mommy
  person%men%bob
  person%men%david
  person%women%eve

Output::

  %
  |-- fruit
  |   |-- apple
  |   |   |-- green
  |   |   |-- red
  |   |   `-- yellow
  |   |-- banana
  |   |   |-- green
  |   |   `-- yellow
  |   `-- orange
  |       |-- green
  |       `-- orange
  |-- i_want_my_mommy
  `-- person
      |-- men
      |   |-- bob
      |   `-- david
      `-- women
          `-- eve

.. _question :http://stackoverflow.com/q/4420878/110488
