#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
:author: Chen Levy <chen (at) pytree.org>
:license: MIT
:date: 2013-03-11
"""
import sys
import itertools

# DISPALY

# expample    ! segments [] ! leaf
# ------------+------+------+------
# a           ! None ! None ! a
# |--- b      ! |--- ! None ! b
# |    `--- c ! |    ! `--- ! c
# `--- d      ! `--- ! None ! d
# stem := segments[-1]
# base := segments[:-1]


def segment_str(more):
    return '|   ' if more else '    '


def stem_str(more):
    return '|-- ' if more else '`-- '


def base_str(base):
    """return a string of the base leading to the leaf"""
    if base:
        return "%s%s" % (
            ''.join(segment_str(is_more) for is_more in base[:-1]),
            stem_str(base[-1]))
    else:
        return ''


# INTERNAL REPRESNETATION
# We define a tree as: $node := (data, [$node*])
# For example:
# Dispaly      ! Representation
# -------------+--------------------------
# a            ! (a,[
# |-- b        !     (b,[
# |   `-- c    !         (c,[])    ]), #/b
# `-- d        !     (d,[])        ])  #/a


_END = object()

def more_items(collection):
    """generator: for a given list yeald a tupple (item,more),

    Where `more` is a boolean that is False only for the last
    `item`.

    """
    iterator = itertools.chain(iter(collection), [_END])
    lookahead = next(iterator)

    while True:
        element = lookahead
        if element is _END:
            break
        lookahead = next(iterator)
        yield (element, lookahead is not _END)



def write_tree(tree, output=sys.stdout, base=[]):
    """write a graphical representation of the tree

    `tree` is the (sub) tree to be processed.

    `output` is a file-like object to write the `tree` to.

    `base` is a list of Booleans: True if there should be more leafs
    on this level of the tree, and False if not,

    """
    stem, leaves = tree
    output.write('%s%s\n' % (base_str(base), stem))
    for (leaf, more) in more_items(leaves):
        write_tree(leaf, output, base + [more])


def merge(tree, other_tree):
    """merge `other_tree` into `tree`"""
    for other_leaf in other_tree[1]:
        leaves = tree[1]
        for leaf in leaves:
            if leaf[0] == other_leaf[0]:
                merge(leaf, other_leaf)
                break
        else:
            leaves.append(other_leaf)


def segments_to_branch(segments):
    """[a,b,c] -> (c,[(b,[(c,[])])])"""
    return (segments[0],
            [segments_to_branch(segments[1:])] if segments[1:] else [])


def create_branch(line, sep='/'):
    """/a/b/c -> (/[(c,[(b,[(c,[])])])])"""
    segments = filter(None, line.strip().split(sep))
    return (sep, [segments_to_branch(segments)] if segments else [])


def read_tree(input=sys.stdin, sep='/', tree=None):
    """return a tree data structure from an iterable bunch of `sep`
    separated strings.

    """
    if tree is None:
        tree = create_branch(sep)
    for line in input:
        merge(tree, create_branch(line))
    return tree


def text_to_tree(input=sys.stdin, output=sys.stdout, sep='/'):
    """convert a iterable separated string into a tree like
    representation

    """
    write_tree( read_tree(input, sep), output )


if __name__ == '__main__':
    text_to_tree()
