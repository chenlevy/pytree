#!/usr/bin/python
# -*- coding: utf-8 -*-

import libtree
import StringIO


class Tree(object):

    def __init__(self, sep='/'):
        self.data = libtree.create_branch('', sep=sep)
        self.sep = sep

    def __add__(self, other_tree):
        try:
            other_data = other_tree.data
        except AttributeError:
            other_data = libtree.create_branch(other_tree, sep=self.sep)
        libtree.merge(self.data, other_data)
        return self

    def __unicode__(self):
        s = StringIO.StringIO()
        libtree.write_tree(self.data, output=s)
        s.seek(0)
        return s.read()

    def __str__(self):
        return self.__unicode__()

    def read(self, input):
        libtree.read_tree(input=input, sep=self.sep, tree=self.data)

    def write(self, output):
        libtree.write_tree(tree=self.data, output=output)
